const csvParser = require('csv-parser');
const fs = require('fs');
const path = require('path');

const matchesDataPath = './src/data/matches.csv';
const deliveriesDataPath = './src/data/deliveries.csv';
const outputPath = './src/public/output';

const matchesPerYear = require('./src/server/1-matches-per-year.cjs');
const matchesWonPerTeamPerYear = require('./src/server/2-matches-won-per-team-per-year.cjs');
const extraRunsConcededPerTeam = require('./src/server/3-extra-runs-per-team.cjs');
const topTenEconomicalBowlers = require('./src/server/4-top-ten-economical-bowlers.cjs');
const teamsWonTossWonMatch = require('./src/server/5-teams-won-toss-won-match.cjs');
const highestPlayerOfTheMatchAwards = require('./src/server/6-highest-player-of-the-match.cjs');
const strikeRateOfBatsmen = require('./src/server/7-strike-rate-of-batsmen.cjs');
const highestTimesOneDismissedAnother = require('./src/server/8-highest-times-one-dismissed-another.cjs');
const bestBowlerEconomySuperOver = require('./src/server/9-bowler-best-ecomony-super-overs.cjs');

const matches = [];
const deliveries = [];

fs.createReadStream(matchesDataPath)
  .pipe(csvParser())
  .on('data', (match) => matches.push(match))
  .on('end', () => {
    fs.createReadStream(deliveriesDataPath)
        .pipe(csvParser())
        .on('data', (ball) => deliveries.push(ball))
        .on('end', () => {

        // 1. Matches Played Per Year
        try{
            const matchesPerYearObj = matchesPerYear(matches);
            fs.writeFile(path.join(outputPath, '1-matches-per-year.json'), JSON.stringify(matchesPerYearObj, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        } catch (err){
            console.error(err);
        }

        // 2. Matches Won Per Year Per Team
        try{
            const totalMatchesWonPerTeamPerYear = matchesWonPerTeamPerYear(matches);
            fs.writeFile(path.join(outputPath, '2-matches-won-per-team-per-year.json'), JSON.stringify(totalMatchesWonPerTeamPerYear, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        } catch (err){
            console.error(err);
        }

        // 3. Extra runs conceded per team in the year 2016
        try{
            const totalExtraRunsConcededPerTeam = extraRunsConcededPerTeam(matches, deliveries);
            fs.writeFile(path.join(outputPath, '3-extra-runs-per-team.json'), JSON.stringify(totalExtraRunsConcededPerTeam, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        }
        catch(err){
            console.error(err);
        }

        // 4. Top 10 economical bowlers in the year 2015
        try{
            const topTenEconomicalBowlersResult = topTenEconomicalBowlers(matches, deliveries);
            fs.writeFile(path.join(outputPath, '4-top-ten-economical-bowlers.json'), JSON.stringify(topTenEconomicalBowlersResult, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        }
        catch(err){
            console.error(err);
        }

        // 5. number of times each team won the toss and also won the match
        try{
            const teamsWonTossWonMatchObj = teamsWonTossWonMatch(matches);
            fs.writeFile(path.join(outputPath, '5-teams-won-toss-won-match.json'), JSON.stringify(teamsWonTossWonMatchObj, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        }
        catch(err){
            console.error(err);
        }

        // 6. highest number of player of the match awards for each season
        try{
            const highestPlayerOfTheMatchResult = highestPlayerOfTheMatchAwards(matches);
            fs.writeFile(path.join(outputPath, '6-highest-player-of-the-match.json'), JSON.stringify(highestPlayerOfTheMatchResult, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        }
        catch(err){
            console.error(err);
        }
        // 7. Strike rate of a batsman for each season
        try{
            const strikeRateOfBatsmenResult = strikeRateOfBatsmen(matches, deliveries, "DA Warner");
            fs.writeFile(path.join(outputPath, '7-strike-rate-of-batsmen.json'), JSON.stringify(strikeRateOfBatsmenResult, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        }
        catch(err){
            console.error(err);
        }

        // 8. Highest number of times one player has been dismissed by another player
        try{
            const playersDismissalRecord = highestTimesOneDismissedAnother(deliveries, 'MG Johnson');
            fs.writeFile(path.join(outputPath, '8-highest-times-one-dismissed-another.json'), JSON.stringify(playersDismissalRecord, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        }
        catch(err){
            console.error(err);
        }

        //9. bowler with the best economy in super overs.
        try{
            const bestBowlerEconomySuperOverResult = bestBowlerEconomySuperOver(deliveries);
            fs.writeFile(path.join(outputPath, '9-bowler-best-ecomony-super-overs.json'), JSON.stringify(bestBowlerEconomySuperOverResult, null, 4), (err) => {
                if(err){
                    console.error(err);
                }
            });
        }
        catch(err){
            console.error(err);
        }
    });
  });
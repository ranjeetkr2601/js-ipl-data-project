function matchesPerYear(matches){

    if(typeof matches !== 'undefined' && Array.isArray(matches)){
        
        const totalMatchesPerYear = matches.reduce(function countMatchPerYear(matchesPerYearObj, match){

            if(match.season in matchesPerYearObj){
                matchesPerYearObj[match.season]++;
            }
            else{
                matchesPerYearObj[match.season] = 1;
            } 

            return matchesPerYearObj;

        }, {});

        //Object containing total matches played every year
        return totalMatchesPerYear; 
    }    
    else{
        return {};
    }
}

module.exports = matchesPerYear;
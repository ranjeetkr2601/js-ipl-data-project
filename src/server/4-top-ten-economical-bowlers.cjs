function topTenEconomicalBowlers(matches, deliveries, year = 2015){

    // Stores top 10 economical bowlers.
    let topTenBowlers = {};
    if(Array.isArray(matches) && Array.isArray(deliveries)){

        // Stores match id of the year passed (2015 by default).
        const matchYearId = {};
        matches.forEach(function getMatchId(match){
            if(Number(match.season) === Number(year)){
                matchYearId[match.id] = match.id;
            }
        });

        // storing stats for every bowler of that year.
        let everyBowlerEconomyObj = deliveries.filter((delivery) => {
            return (delivery.match_id in matchYearId);
        }).reduce(function getBowlerStats(bowlerStats, delivery){

            if(!(delivery.bowler in bowlerStats)){
                
                bowlerStats[delivery.bowler] = {
                    runs : 0,
                    balls : 0
                }

            }
            // Increment ball count if its not wide and not no ball
            if(Number(delivery.wide_runs) === 0 && Number(delivery.noball_runs) === 0){
                bowlerStats[delivery.bowler].balls++;
            }
            // updating runs
            bowlerStats[delivery.bowler].runs += (Number(delivery.total_runs) - Number(delivery.bye_runs) - Number(delivery.legbye_runs) - Number(delivery.penalty_runs));

            return bowlerStats;

        }, {});

        //calculating economy for every bowler.
        for(const bowler in everyBowlerEconomyObj){
            everyBowlerEconomyObj[bowler] = Number(everyBowlerEconomyObj[bowler].runs / ((everyBowlerEconomyObj[bowler].balls) / 6)).toFixed(2);
        }

        topTenBowlers = Object.entries(everyBowlerEconomyObj).sort((bowler1, bowler2) => {
            return bowler1[1] - bowler2[1];
        }).slice(0,10).reduce((topTenBowler, currentBowler, index) => {
            let bowler = currentBowler[0];
            let economy = currentBowler[1];

            topTenBowler[index + 1] = {
                "name" : bowler,
                "economy" : economy
            };
            return topTenBowler;

        }, {});
    }

        return topTenBowlers;
}

module.exports = topTenEconomicalBowlers;
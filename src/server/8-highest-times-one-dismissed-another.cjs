function highestTimesOneDismissedAnother(deliveries, batsmanToFind = 'all', bowlerToFind = 'all'){

    let playerDismissalRecord = {};

    if(Array.isArray(deliveries)){

        let batsman = batsmanToFind;
        let bowler = bowlerToFind;
        
        // Object playerDismissalRecord stores Number of times a batsman is dismissed by a bowler.
        playerDismissalRecord = deliveries.reduce((playerDismissedObj, delivery) => {

            if(batsmanToFind === 'all'){
                batsman = delivery.batsman;
            }
            if(bowlerToFind === 'all'){
                bowler = delivery.bowler;
            }
            if(batsman === delivery.player_dismissed && bowler === delivery.bowler && delivery.dismissal_kind !== 'run out'){
                
                if(!(batsman in playerDismissedObj)){
                    playerDismissedObj[batsman] = {};
                }
                if(!(delivery.bowler in playerDismissedObj[batsman])){
                    playerDismissedObj[batsman][delivery.bowler] = 0;
                }
                playerDismissedObj[batsman][delivery.bowler]++;
            }

            return playerDismissedObj;

        }, {});
    }

    return playerDismissalRecord;
    
}

module.exports = highestTimesOneDismissedAnother;
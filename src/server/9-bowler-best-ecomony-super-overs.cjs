function bestBowlerEconomySuperOver(deliveries){

    function calcEconomy(runs, overs){
        return Number((runs / overs).toFixed(2));
    }

    let bestEconomyBowler = {};

    if(Array.isArray(deliveries)){

        //bowlerRunsAndBalls- stores runs and balls as array elements for every bowler in super over.
        let bowlerRunsAndBalls = deliveries.reduce((bowlerStats, delivery) => {
            //runsOn delivery- stores runs on the delivery conceded by bowler.
            let runsOnDelivery = Number(delivery.total_runs) - Number(delivery.legbye_runs) - Number(delivery.bye_runs);

            if(Number(delivery.is_super_over) === 1){

                if(!(delivery.bowler in bowlerStats)){
                    bowlerStats[delivery.bowler] = {
                        runs : 0,
                        balls : 0
                    }
                }
                bowlerStats[delivery.bowler].runs += runsOnDelivery;

                if(Number(delivery.wide_runs) === 0 && Number(delivery.noball_runs) === 0){
                    bowlerStats[delivery.bowler].balls++;
                }
                
            }

            return bowlerStats; 

        }, {});

        for(const bowler in bowlerRunsAndBalls){
            bowlerRunsAndBalls[bowler] = calcEconomy(bowlerRunsAndBalls[bowler].runs, bowlerRunsAndBalls[bowler].balls / 6);
        }

        let leastEconomy = Object.values(bowlerRunsAndBalls).reduce((minEconomy, currentEconomy) => {
            if(Number(minEconomy) > Number(currentEconomy)){
                minEconomy = currentEconomy;
            }
            return minEconomy;
        }, Infinity);

        bestEconomyBowler = Object.fromEntries(Object.entries(bowlerRunsAndBalls).filter(([bowler, economy]) => {
            return economy === leastEconomy;
        }));
    }
    return bestEconomyBowler;
}

module.exports = bestBowlerEconomySuperOver;
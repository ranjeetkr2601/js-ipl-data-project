function strikeRateOfBatsmen(matches, deliveries, batsmanToFind = "all") {

    function calcStrikeRate(runs, balls) {
        return ((runs / balls) * 100).toFixed(2);
    }

    let strikeRateOfBatsmen = {};

    if (Array.isArray(matches) && Array.isArray(deliveries)) {
        let batsman = batsmanToFind;
        // stores Id and season as key & value
        const matchYearById = {};
        matches.forEach((match) => {
            if (!(match.id in matchYearById)) {
                matchYearById[match.id] = match.season;
            }
        });

        strikeRateOfBatsmen = deliveries.reduce((runsAndBallsCount, delivery) => {

            if(batsmanToFind === 'all'){
                batsman = delivery.batsman;
            }
            if(batsman === delivery.batsman){

                if (!(batsman in runsAndBallsCount)) {
                    runsAndBallsCount[delivery.batsman] = {};
                }

                if (!(matchYearById[delivery.match_id] in runsAndBallsCount[batsman])) {
                    runsAndBallsCount[delivery.batsman][matchYearById[delivery.match_id]] =
                    {
                        runs: 0,
                        balls: 0,
                    };
                }

                if (Number(delivery.wide_runs) === 0) {
                    runsAndBallsCount[delivery.batsman][matchYearById[delivery.match_id]]
                    .balls++;
                }

                runsAndBallsCount[delivery.batsman][
                    matchYearById[delivery.match_id]
                ].runs += Number(delivery.batsman_runs);
            }
            return runsAndBallsCount;

        }, {});

        //changing strikeRateOfBatsmen[batsman][year] value from {runs,balls} to strike rate.
        for (const batsman in strikeRateOfBatsmen) {
            for (const year in strikeRateOfBatsmen[batsman]) {
                strikeRateOfBatsmen[batsman][year] = Number(
                calcStrikeRate(
                    strikeRateOfBatsmen[batsman][year].runs,
                    strikeRateOfBatsmen[batsman][year].balls
                    )   
                );
            }
        }
    }

  return strikeRateOfBatsmen;
}

module.exports = strikeRateOfBatsmen;

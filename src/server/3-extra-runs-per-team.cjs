function extraRunsConcededPerTeam(matches, deliveries, year = 2016){

    //stores total extra runs of a particular year.
    let totalExtraRunsPerTeam = {};

    if(Array.isArray(matches) && Array.isArray(deliveries)){

        //Stores match ID of all the matches of the particular year.
        const matchYearId = {};

        matches.forEach(function getMatchId(match){
                if(Number(match.season) === Number(year)){
                    matchYearId[match.id] = match.id;
                }
        });
        
        totalExtraRunsPerTeam = deliveries.reduce(function calcExtraRuns(extraRunsObj, delivery){
            if(delivery.match_id in matchYearId){
                if(delivery.bowling_team in extraRunsObj){
                    extraRunsObj[delivery.bowling_team] += Number(delivery.extra_runs);
                }
                else{
                    extraRunsObj[delivery.bowling_team] = Number(delivery.extra_runs);
                }
            }
            return extraRunsObj;
        }, {});
    }

    return totalExtraRunsPerTeam;
}

module.exports = extraRunsConcededPerTeam;
function highestPlayerOfTheMatchAwards(matches){

    let highestPlayerOfTheMatchPerSeason = {};

    if(Array.isArray(matches)){
        
        const allPlayersOfTheMatchPerSeason = matches.reduce(function countPlayerOfTheMatches(playerOfTheMatchObj, match){

            if(!(match.season in playerOfTheMatchObj)){
                playerOfTheMatchObj[match.season] = {};
            }
            if(!(match.player_of_match in playerOfTheMatchObj[match.season])){
                playerOfTheMatchObj[match.season][match.player_of_match] = 0;
            }
            
            playerOfTheMatchObj[match.season][match.player_of_match]++;
        
            return playerOfTheMatchObj;

        }, {});

        highestPlayerOfTheMatchPerSeason = Object.entries(allPlayersOfTheMatchPerSeason).reduce((highestPlayerPerSeason, [season, playerOfTheMatchEachSeason]) => {

            // playerOfTheMatchEachSeason contains all players from one season
            // countAwards contains Player Of The Match awards count of each player of a season 
            const countAwards = Object.values(playerOfTheMatchEachSeason);

            // Finding the highest entry in countAwards.
            const highestAwards = countAwards.reduce((maxCount, currentCount) => {
                if(maxCount < currentCount){
                    maxCount = currentCount;
                }
                return maxCount;
            });

            // filtering playerOfTheMatchEachSeason when its corresponding awardCounts === highestAwards
            highestPlayerPerSeason[season] = Object.fromEntries(Object.entries(playerOfTheMatchEachSeason).filter(([playerName, awardCounts]) => {
                return highestAwards === awardCounts;
            }));

            return highestPlayerPerSeason;

        }, {});
        
    }    
    
    return highestPlayerOfTheMatchPerSeason;
}
module.exports = highestPlayerOfTheMatchAwards;
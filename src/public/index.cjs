//1. Matches Per Year
fetch("./output/1-matches-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    const matchesPerYearData = Object.values(data);

    Highcharts.chart("1-matches-per-year", {
      chart: {
        type: "cylinder",
        options3d: {
          enabled: true,
          alpha: 10,
          beta: 10,
          depth: 60,
          viewDistance: 50,
        },
      },
      title: {
        margin: 20,
        text: "1. Matches Per Year in IPL",
      },
      xAxis: {
        title: {
          margin: 20,
          text: "Years",
        },
      },
      yAxis: {
        title: {
          margin: 30,
          text: "Number of Matches",
        },
      },
      tooltip: {
        headerFormat: "<b>Year: {point.x}</b><br>",
      },
      plotOptions: {
        series: {
          depth: 50,
          colorByPoint: true,
          pointStart: 2008,
        },
      },
      series: [
        {
          data: matchesPerYearData,
          name: "Matches",
          showInLegend: false,
        },
      ],
    });
  });

// 2. Matches Won Per Team Per Year

fetch("./output/2-matches-won-per-team-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    let years = Object.values(data)
      .map((yearWiseWins) => {
        return Object.keys(yearWiseWins);
      })
      .flat()
      .filter((year, index, self) => {
        return self.indexOf(year) === index;
      });
    let dataArray = [];

    Object.entries(data).map(([team, yearWiseWins]) => {
      let dataObj = {};
      dataObj["name"] = team;
      years.map((year) => {
        if (!(year in yearWiseWins)) {
          yearWiseWins[year] = "Did Not Play/Win";
        }
      });
      dataObj["data"] = Object.values(yearWiseWins);
      dataArray.push(dataObj);
    });

    Highcharts.chart("2-matches-won-per-team-per-year", {
      chart: {
        type: "column",
        options3d: {
          enabled: false,
          alpha: 10,
          beta: 10,
          depth: 60,
          viewDistance: 50,
        },
      },
      title: {
        margin: 20,
        text: "2. Matches Won Per Team Per Year",
      },
      xAxis: {
        title: {
          margin: 20,
          text: "Years",
        },
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          margin: 30,
          text: "Number of wins",
        },
      },
      tooltip: {
        headerFormat: "<b>Year: {point.x}</b><br>",
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
        series: {
          depth: 50,
          // colorByPoint: true,
          pointStart: 2008,
        },
      },
      series: dataArray,
    });
  });

//3. Extra runs per team
fetch("./output/3-extra-runs-per-team.json")
  .then((data) => data.json())
  .then((data) => {
    const extraRunsPerTeam = Object.entries(data);

    Highcharts.chart("3-extra-runs-per-team", {
      chart: {
        type: "cylinder",
        options3d: {
          enabled: true,
          alpha: 10,
          beta: 10,
          depth: 60,
          viewDistance: 50,
        },
      },
      title: {
        margin: 20,
        text: "3. Extra Runs Per Team",
      },
      xAxis: {
        type: "category",
        title: {
          margin: 20,
          text: "Team",
        },
      },
      yAxis: {
        options3d: {
          enabled: true,
          alpha: 20,
          beta: 30,
        },
        title: {
          margin: 30,
          text: "Extra Runs",
        },
      },
      tooltip: {
        // headerFormat: '<b>Team: {point.x}</b><br>'
      },
      plotOptions: {
        series: {
          depth: 50,
          colorByPoint: true,
        },
      },
      series: [
        {
          data: extraRunsPerTeam,
          name: "",
          showInLegend: false,
        },
      ],
    });
  });

//4. Top Ten Economical bowlers
fetch("./output/4-top-ten-economical-bowlers.json")
  .then((data) => data.json())
  .then((data) => {
    const bowlerEconomy = Object.values(data);
    let dataToPlot = [];
    bowlerEconomy.map((currentPlayer) => {
      dataToPlot.push([currentPlayer.name, Number(currentPlayer.economy)]);
    });

    Highcharts.chart("4-top-ten-economical-bowlers", {
      chart: {
        type: "cylinder",
        options3d: {
          enabled: true,
          alpha: 10,
          beta: 10,
          depth: 60,
          viewDistance: 50,
        },
      },
      title: {
        margin: 20,
        text: "4. Top Ten Economical Bowlers in 2015",
      },
      xAxis: {
        type: "category",
        title: {
          margin: 20,
          text: "Players",
        },
      },
      yAxis: {
        title: {
          margin: 30,
          text: "Economy",
        },
      },
      tooltip: {
        // headerFormat: '<b>Team: {point.x}</b><br>'
      },
      plotOptions: {
        series: {
          depth: 50,
          colorByPoint: true,
        },
      },
      series: [
        {
          data: dataToPlot,
          name: "",
          showInLegend: false,
        },
      ],
    });
  });

//5. Teams Won Toss Won Match
fetch("./output/5-teams-won-toss-won-match.json")
  .then((data) => data.json())
  .then((data) => {
    const teamsWon = Object.entries(data);

    Highcharts.chart("5-teams-won-toss-won-match", {
      chart: {
        type: "cylinder",
        options3d: {
          enabled: true,
          alpha: 10,
          beta: 10,
          depth: 60,
          viewDistance: 50,
        },
      },
      title: {
        margin: 20,
        text: "5. Teams Won Toss Won Match",
      },
      xAxis: {
        type: "category",
        title: {
          margin: 20,
          text: "Teams",
        },
      },
      yAxis: {
        options3d: {
          enabled: true,
          alpha: 20,
          beta: 30,
        },
        title: {
          margin: 30,
          text: "Matches Won",
        },
      },
      plotOptions: {
        series: {
          depth: 50,
          colorByPoint: true,
        },
      },
      series: [
        {
          data: teamsWon,
          name: "",
          showInLegend: false,
        },
      ],
    });
  });

//6. Highest player of the match
fetch("./output/6-highest-player-of-the-match.json")
  .then((data) => data.json())
  .then((data) => {
    const years = Object.keys(data);
    const playersYearWise = Object.values(data);
    let dataToPlot = [];
    playersYearWise.map((currentPlayer) => {
      let arr = [];
      dataToPlot.push([
        Object.keys(currentPlayer).toString(),
        Number(Object.values(currentPlayer).toString()),
      ]);
    });

    Highcharts.chart("6-highest-player-of-the-match", {
      chart: {
        type: "cylinder",
        options3d: {
          enabled: true,
          alpha: 10,
          beta: 10,
          depth: 60,
          viewDistance: 50,
        },
      },
      title: {
        margin: 20,
        text: "6. Highest Player Of The Match Awards",
      },
      xAxis: {
        categories: years,
        title: {
          margin: 20,
          text: "Years",
        },
      },

      yAxis: {
        title: {
          margin: 30,
          text: "Awards Won",
        },
      },
      plotOptions: {
        series: {
          depth: 50,
          colorByPoint: true,
        },
      },
      series: [
        {
          data: dataToPlot,
          name: "",
          showInLegend: false,
        },
      ],
    });
  });

//7. 7-strike-rate-of-batsmen
fetch("./output/7-strike-rate-of-batsmen.json")
  .then((data) => data.json())
  .then((data) => {
    let years = Object.values(data)
      .map((yearWiseStrike) => {
        return Object.keys(yearWiseStrike);
      })
      .flat()
      .filter((year, index, self) => {
        return self.indexOf(year) === index;
      });
      
    let dataArray = [];

    Object.entries(data).map(([player, yearWiseStrike]) => {
      let dataObj = {};
      dataObj["name"] = player;
      years.map((year) => {
        if (!(year in yearWiseStrike)) {
          yearWiseStrike[year] = "Did Not Play";
        }
      });
      dataObj["data"] = Object.values(yearWiseStrike);
      dataArray.push(dataObj);
    });

    Highcharts.chart("7-strike-rate-of-batsmen", {
      chart: {
        type: "cylinder",
        options3d: {
          enabled: true,
          alpha: 10,
          beta: 10,
          depth: 60,
          viewDistance: 50,
        },
      },
      title: {
        margin: 20,
        text: "7. Strike Rate Of Batsman Each Year",
      },
      xAxis: {
        categories: years,
        title: {
          margin: 20,
          text: "Years",
        },
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          margin: 30,
          text: "Number of wins",
        },
      },
      tooltip: {
        headerFormat: "<b>Year: {point.x}</b><br>",
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
        series: {
          depth: 50,
          colorByPoint: true,
        },
      },
      series: dataArray,
    });
  });

//8. Highest Times One dismissed Another
fetch("./output/8-highest-times-one-dismissed-another.json")
.then((data) => data.json())
.then((data) => {
  
  let bowlersArray = Object.values(data)
      .map((bowler) => {
        return Object.keys(bowler);
      })
      .flat()
      .filter((bowler, index, self) => {
        return self.indexOf(bowler) === index;
      });
    let dataArray = [];

    Object.entries(data).map(([player, bowlers]) => {
      let dataObj = {};
      dataObj["name"] = player;
      bowlersArray.map((bowler) => {
        if (!(bowler in bowlers)) {
          bowlers[bowler] = "Did Not Out";
        }
      });
      dataObj["data"] = Object.values(bowlers);
      dataArray.push(dataObj);
    });

  Highcharts.chart("8-highest-times-one-dismissed-another", {
    chart: {
      type: "cylinder",
      options3d: {
        enabled: true,
        alpha: 10,
        beta: 10,
        depth: 60,
        viewDistance: 50,
      },
    },
    title: {
      margin: 20,
      text: "8. Highest Times One Dismissed Another",
    },
    xAxis: {
      categories: bowlersArray,
      title: {
        margin: 20,
        text: "Bowlers",
      },
    },

    yAxis: {
      title: {
        margin: 30,
        text: "No. of times Dismissed",
      },
    },
    plotOptions: {
      series: {
        depth: 50,
        colorByPoint: true,
      },
    },
    series: dataArray
  });
});

//9. Bowler with best economy in super overs.
fetch("./output/9-bowler-best-ecomony-super-overs.json")
.then((data) => data.json())
.then((data) => {

  let bowler = Object.keys(data).toString();
  let economy = Object.values(data);

  Highcharts.chart("9-bowler-best-economy-super-overs", {
    chart: {
      type: "cylinder",
      options3d: {
        enabled: true,
        alpha: 10,
        beta: 10,
        depth: 60,
        viewDistance: 50,
      },
    },
    title: {
      margin: 20,
      text: "9. Bowler Best Economy Super Overs",
    },
    xAxis: {
      categories:[bowler],
      title: {
        margin: 20,
        text: "Bowler",
      },
    },

    yAxis: {
      title: {
        margin: 30,
        text: "Economy",
      },
    },
    plotOptions: {
      series: {
        depth: 50,
        colorByPoint: true,
      },
    },
    series: [{
      name : bowler,
      data : economy
    }]
  });
});